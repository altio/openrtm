from io import BytesIO
from xlsxwriter import Workbook
from openrtm.products import models


class Customer(models.AbbreviationModel):
    products = models.ManyToManyField(
        to=models.products.Product,
        through='Stakeholder',
        blank=True,
        related_name='customers',
    )


class CustomerModel(models.Model):
    customer = models.ForeignKey(
        to=Customer,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
    )

    class Meta:
        abstract = True


class StakeholderQuerySet(models.QuerySet):
    def generate_rtm(self, admin, request):
        output = BytesIO()
        workbook = Workbook(output, options=dict(in_memory=True))
        rtm = workbook.add_worksheet('RTM')

        # write header row
        headers = [
            'Customer',
            'Req. ID',
            'Requirement',
            'Story ID',
            'Story',
            'Acc. ID',
            'Acceptance Criteria',
        ]
        for col in range(len(headers)):
            rtm.write(0, col, headers[col])

        # write rtm data rows
        row = 1
        for stakeholder in self:
            for requirement in stakeholder.requirements.order_by('identifier'):
                for story in requirement.stories.order_by('identifier'):
                    for acceptance_criterion in story.acceptance_criteria.order_by('identifier'):
                        rtm.write(row, 0, stakeholder.customer.name)
                        rtm.write(row, 1, requirement.unique_identifier)
                        rtm.write(row, 2, requirement.description)
                        rtm.write(row, 3, story.unique_identifier)
                        rtm.write(row, 4, story.description)
                        rtm.write(row, 5, acceptance_criterion.unique_identifier)
                        rtm.write(row, 6, acceptance_criterion.description)
                        row += 1

        """
        # Could optimize query...
        for stakeholder in self:
            acceptance_criteria = models.products.AcceptanceCriterion.objects.filter(
                story__stakeholders=stakeholder,
            ).order_by(
                'story__requirements__identifier',
                'story__identifier',
                'identifier',
            ).values_list(
                'story__requirements__identifier',
                'story__requirements__description',
                'story__identifier',
                'story__description',
                'identifier',
                'description',
            )
        """

        # future work
        # mappings = Mapping.objects.filter(stakeholder__in=self)
        # stories = models.products.Story.objects.filter(mappings__in=mappings).distinct()
        # stories = workbook.add_worksheet('Stories')
        workbook.close()
        output.seek(0)
        return output


class StakeholderManager(type(Customer.objects).from_queryset(StakeholderQuerySet)):
    use_for_related_fields = True


class Stakeholder(CustomerModel, models.ProductModel):

    objects = StakeholderManager()

    stories = models.ManyToManyField(
        to=models.products.Story,
        through='Mapping',
        blank=True,
        related_name='stakeholders',
    )

    def __str__(self):
        return '{} on {}'.format(self.customer, self.product)


class StakeholderModel(CustomerModel, models.ProductModel):
    customer = models.ForeignKey(
        to=Customer,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
        editable=False,
    )
    product = models.ForeignKey(
        to=models.products.Product,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
        editable=False,
    )
    stakeholder = models.ForeignKey(
        to=Stakeholder,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
    )

    def clean(self):
        if not hasattr(self, 'customer'):
            self.customer = self.stakeholder.customer
        if not hasattr(self, 'product'):
            self.product = self.stakeholder.product
        super(StakeholderModel, self).clean()
        assert self.customer == self.stakeholder.customer
        assert self.product == self.stakeholder.product

    class Meta:
        abstract = True


class Requirement(StakeholderModel, models.UniqueIdentifierModel):
    IDENTIFIER_GROUP_FIELD_NAME = 'customer'
    description = models.TextField()
    stories = models.ManyToManyField(
        to=models.products.Story,
        through='Mapping',
        blank=True,
        related_name='requirements',
    )


class RequirementModel(StakeholderModel):
    stakeholder = models.ForeignKey(
        to=Stakeholder,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
        editable=False,
    )
    requirement = models.ForeignKey(
        to=Requirement,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
    )

    def clean(self):
        if not hasattr(self, 'stakeholder'):
            self.stakeholder = self.requirement.stakeholder
        super(RequirementModel, self).clean()
        assert self.stakeholder == self.requirement.stakeholder

    class Meta:
        abstract = True


class Mapping(RequirementModel, models.StoryModel):
    pass
