from django.core.management.base import (
    no_translations,
)
from faker import Faker
from openrtm.products.management.commands.make_fixtures import Command
from openrtm.products.models import products as products_models
from ... import models


fake = Faker()
fake.seed_instance(1)  # constant data


def make_fixtures():
    product_ids = [
        pk for pk in products_models.Product.objects.all().values_list('pk', flat=True)
    ]

    # make customers
    for _ in range(5):
        name = fake.company()
        abbr = ''.join(part[0].upper() for part in name.replace('-', ' ').split(' '))
        customer = models.Customer.objects.create(name=fake.company(), abbreviation=abbr)

        # make customer a stakeholder on some projects
        customer_product_ids = fake.random.sample(
            product_ids,
            fake.random.randint(1, 3)
        )
        for product_id in customer_product_ids:
            stakeholder = models.Stakeholder.objects.create(
                customer=customer,
                product_id=product_id,
            )

            product_story_ids = [
                pk for pk in products_models.Story.objects.filter(
                    product_id=product_id,
                ).values_list('pk', flat=True)
            ]

            # make stakeholder requirements
            for _ in range(1, fake.random.randint(2, 3)):
                requirement = models.Requirement.objects.create(
                    stakeholder=stakeholder,
                    description=fake.sentence(),
                )

                # map requirement to stories via mapping
                requirement_story_ids = fake.random.sample(
                    product_story_ids,
                    fake.random.randint(1, 3),
                )
                for story_id in requirement_story_ids:
                    models.Mapping.objects.create(
                        requirement=requirement,
                        story_id=story_id,
                    )


class Command(Command):
    help = "Creates fixtures for the customers app."

    @no_translations
    def handle(self, *app_labels, **options):
        super(Command, self).handle(*app_labels, **options)
        make_fixtures()
