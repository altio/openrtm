from django.contrib import admin
from django.http.response import HttpResponse
from openrtm.products import admin as products_admin
from openrtm.customers import models


class MappingInline(admin.TabularInline):
    model = models.Mapping
    extra = 0
    fields = ('requirement', 'story')
    readonly_fields = ()


class RequirementInline(admin.TabularInline):
    model = models.Requirement
    extra = 0
    fields = ('identifier', 'description')
    readonly_fields = ()


@admin.register(models.Requirement)
class RequirementAdmin(admin.ModelAdmin):
    fields = ('stakeholder', 'description')
    inlines = (
        # planning_admin.AcceptanceCriteriaInline,
    )


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    fields = ('name', 'abbreviation')
    list_display = ('id', 'name', 'abbreviation')
    inlines = (
        #RequirementInline,
    )


@admin.register(models.Stakeholder)
class StakeholderAdmin(admin.ModelAdmin):
    fields = ('product', 'customer')
    list_display = ('id', 'product', 'customer')
    inlines = (
        RequirementInline,
    )
    actions = (
        'generate_rtm',
    )

    def generate_rtm(self, request, queryset):
        filename = 'rtm'
        xlsx_bytes = queryset.generate_rtm(self, request)
        response = HttpResponse(
            content=xlsx_bytes.read(),
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        )
        response['Content-Disposition'] = f'attachment; filename={filename}.xlsx'
        return response

    generate_rtm.short_description = "Generate RTM for selected"


class StakeholderInline(admin.TabularInline):
    model = models.Stakeholder
    verbose_name_plural = "Stakeholders with an interest in this product"
    extra = 0
    fields = ('name', 'abbreviation', )
    readonly_fields = ()
