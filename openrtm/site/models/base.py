import uuid
from django.db import models

__all__ = ('Model', 'BaseModel', 'NameModel', 'AbbreviationModel', 'UniqueIdentifierModel')


class Model(models.Model):
    def save(self, *args, **kwargs):
        self.full_clean()
        super(Model, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class BaseModel(Model):
    uuid = models.UUIDField(editable=False, default=uuid.uuid4)

    def __str__(self):
        return self.uuid

    class Meta:
        abstract = True


class NameModel(BaseModel):
    name = models.CharField(max_length=60)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class AbbreviationModel(NameModel):
    abbreviation = models.CharField(max_length=5, blank=True)

    def clean(self):
        super(AbbreviationModel, self).clean()
        if not self.abbreviation:
            self.abbreviation = self.name.upper()[:3]

    class Meta:
        abstract = True


class UniqueIdentifierModel(Model):

    IDENTIFIER_GROUP_FIELD_NAME = None

    identifier = models.PositiveIntegerField(blank=True)

    @property
    def identifier_group(self):
        return getattr(self, self.IDENTIFIER_GROUP_FIELD_NAME)

    @property
    def unique_identifier(self):
        return '{}-{:04d}'.format(
            self.identifier_group.abbreviation,
            self.identifier,
        )

    def clean(self):
        super(UniqueIdentifierModel, self).clean()
        if not self.identifier:
            self.identifier = (self._meta.model.objects.filter(**{
                self.IDENTIFIER_GROUP_FIELD_NAME: self.identifier_group,
            }).aggregate(models.Max('identifier'))['identifier__max'] or 0) + 1

    def __str__(self):
        return self.unique_identifier

    class Meta:
        abstract = True
