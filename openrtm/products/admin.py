from django import forms
from django.contrib import admin
from . import models


class DescriptionMixin(object):

    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(DescriptionMixin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'description':
            formfield.widget.attrs['rows'] = '1'
        return formfield


class AcceptanceCriteriaMixin(DescriptionMixin):
    model = models.products.AcceptanceCriterion
    fields = ('story', 'identifier', 'description')
    readonly_fields = ()


@admin.register(models.products.AcceptanceCriterion)
class AcceptanceCriteriaAdmin(AcceptanceCriteriaMixin, admin.ModelAdmin):
    list_display = ('__str__', 'story', 'identifier', 'description')
    list_editable = ('story', 'identifier', 'description')
    list_filter = ('story',)
    search_fields = ('identifier', 'description')


class AcceptanceCriteriaInline(AcceptanceCriteriaMixin, admin.TabularInline):
    extra = 0


class StoryMixin(DescriptionMixin):
    model = models.products.Story
    fields = ('product', 'identifier', 'description', )
    readonly_fields = ()


@admin.register(models.products.Story)
class StoryAdmin(StoryMixin, admin.ModelAdmin):
    list_display = ('__str__', 'product', 'identifier', 'description')
    list_editable = ('product', 'identifier', 'description')
    list_filter = ('product',)
    search_fields = ('identifier', 'description')
    inlines = (
        AcceptanceCriteriaInline,
    )


class StoryInline(StoryMixin, admin.TabularInline):
    extra = 0


@admin.register(models.products.Release)
class ReleaseAdmin(admin.ModelAdmin):
    fields = ('product', 'number', 'date_anticipated')
    inlines = (
        StoryInline,
    )


class ReleaseInline(admin.TabularInline):
    model = models.products.Release
    extra = 0
    fields = ('product', 'number', 'date_anticipated')
    readonly_fields = ()


@admin.register(models.products.Product)
class ProductAdmin(admin.ModelAdmin):
    fields = ('name', 'abbreviation')
    inlines = (
        ReleaseInline,
        StoryInline,
    )
