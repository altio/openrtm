from datetime import timedelta
from django.core.management.base import (
    BaseCommand, no_translations,
)
from django.core.management import call_command
from faker import Faker
from ... import models


fake = Faker()
fake.seed_instance(1)  # constant data


def make_fixtures():
    # make products
    for _ in range(5):
        name = fake.catch_phrase()
        abbr = ''.join(part[0].upper() for part in name.replace('-', ' ').split(' '))
        product = models.products.Product.objects.create(
            name=name,
            abbreviation=abbr,
        )

        # make three releases per product
        date = fake.future_date()
        for ver in range(1, 4):
            release = models.products.Release.objects.create(
                product=product,
                number=f'v{ver}',
                date_anticipated=date,
            )
            date += timedelta(days=90)

            # make a few stories per product
            for _ in range(5):
                story = models.products.Story.objects.create(
                    product=product,
                    release=release,  # release does not imply product for a story
                    description=fake.sentence(),
                )

                # and finally a few acceptance criteria
                for _ in range(fake.random.randint(1, 3)):
                    models.products.AcceptanceCriterion.objects.create(
                        story=story,
                        description=fake.sentence(),
                    )


class Command(BaseCommand):
    help = "Creates fixtures for the products app."

    @no_translations
    def handle(self, *app_labels, **options):
        call_command('flush', interactive=False)
        make_fixtures()
