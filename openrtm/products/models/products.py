from openrtm.site import models

__all__ = ('ProductModel', 'ReleaseModel', 'StoryModel')


class Product(models.AbbreviationModel):
    pass


class ProductModel(models.Model):
    product = models.ForeignKey(
        to=Product,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
    )

    class Meta:
        abstract = True


class Release(ProductModel):
    number = models.CharField(max_length=60)
    date_anticipated = models.DateField(blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.product, self.number)


class ReleaseModel(ProductModel):
    product = models.ForeignKey(
        to=Product,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
        editable=False,
    )
    release = models.ForeignKey(
        to=Release,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
    )

    def clean(self):
        if not hasattr(self, 'product'):
            self.product = self.release.product
        super(ReleaseModel, self).clean()
        assert self.product == self.release.product

    class Meta:
        abstract = True


class Story(models.UniqueIdentifierModel, ProductModel):
    IDENTIFIER_GROUP_FIELD_NAME = 'product'
    product = models.ForeignKey(
        to=Product,
        on_delete=models.CASCADE,
        related_name='stories',
    )
    release = models.ForeignKey(
        to=Release,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
        blank=True,
        null=True,
    )
    description = models.TextField()

    def clean(self):
        super(Story, self).clean()
        if self.release:
            assert self.product == self.release.product

    class Meta:
        verbose_name_plural = 'stories'


class StoryModel(ProductModel):
    product = models.ForeignKey(
        to=Product,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
        editable=False,
    )
    story = models.ForeignKey(
        to=Story,
        on_delete=models.CASCADE,
        related_name='%(class)ss',
    )

    def clean(self):
        if not hasattr(self, 'product'):
            self.product = self.story.product
        super(StoryModel, self).clean()
        assert self.product == self.story.product

    class Meta:
        abstract = True


class AcceptanceCriterion(models.UniqueIdentifierModel, StoryModel):
    IDENTIFIER_GROUP_FIELD_NAME = 'product'
    product = models.ForeignKey(
        to=Product,
        on_delete=models.CASCADE,
        related_name='+',
        editable=False,
    )
    story = models.ForeignKey(
        to=Story,
        on_delete=models.CASCADE,
        related_name='acceptance_criteria',
    )
    description = models.TextField()

    class Meta:
        verbose_name_plural = 'acceptance criteria'
