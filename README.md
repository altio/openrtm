# openrtm
An open-source Requirements Traceability Matrix (RTM) solution.

One-time setup:
  ```
  docker-compose run --rm openrtm pipenv sync --dev
  docker-compose run --rm openrtm pipenv run python manage.py reset_db
  docker-compose run --rm openrtm pipenv run python manage.py migrate
  docker-compose run --rm openrtm pipenv run python manage.py createsuperuser
  docker-compose up -d
  ```

Routine stuff:
  ```
  docker-compose exec openrtm pipenv run python manage.py makemigrations
  ...
  ```
